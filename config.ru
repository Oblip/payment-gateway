# frozen_string_literal: true

require_relative 'require_app.rb'
require_app

run PaymentGateway::App.freeze.app
