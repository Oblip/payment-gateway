# frozen_string_literal: true

require 'roda'
require 'econfig'
require_app('lib')

module PaymentGateway
  # Configuration for the API
  class App < Roda
    plugin :environments

    extend Econfig::Shortcut
    Econfig.env = environment.to_s
    Econfig.root = '.'

    configure :development, :test do
      require 'pry' # allow binding.pry breakpoints

      # Allows running reload! in pry to restart entire app
      def self.reload!
        exec 'pry -r ./specs/test_load_all'
      end
    end

    configure :development, :test do
      # Example:postgres://YourUserName:YourPassword@YourHost:5432/YourDatabase
      ENV['DATABASE_URL'] = config.DATABASE_URL
    end

    # For all runnable environments
    configure do
      require 'sequel'

      Sequel.default_timezone = :utc

      begin
        retries ||= 0
        DB = Sequel.connect(ENV['DATABASE_URL'])
      rescue StandardError => e
        puts 'Could not connect to db. Will retry in 10 seconds'
        sleep(10)
        retry if (retries += 1) < 10
        raise StandardError, e.message
      end

      def self.DB
        DB
      end

      SecureDB.setup(config.DB_KEY) # Load crypto keys
    end
  end
end
