# frozen_string_literal: true

require 'roda'
require 'slim'
require 'slim/include'
require 'rack/ssl-enforcer'
require 'rack/cors'
require 'repost'
require 'faye'
require 'eventmachine'
require 'concurrent'

module PaymentGateway
  # Base class for Payment Gateway Web Application
  class App < Roda
    use Rack::SslEnforcer if ENV['RACK_ENV'] == 'production'
    # use Rack::Cors do
    #   allow do
    #     origins '*'
    #   end
    # end

    plugin :render, engine: 'slim', views: 'app/presentation/views'
    plugin :assets, css: ['style.css'], path: 'app/presentation/assets'
    plugin :assets, js: ['qrCodeWithLogo.min.js', 'renderQrCode.js'],
      path: 'app/presentation/assets'
    plugin :public, root: 'app/presentation/public'

    compile_assets if ENV['RACK_ENV'] == 'production'

    route do |routing|
      routing.public
      routing.assets

      @client = Faye::Client.new("#{App.config.OBLIP_API}/faye")

      routing.root do
        view :home
      end

      routing.on('pay') do

        routing.is do
          routing.post do
            params = routing.params
  
            result = ValidateAccess.new.call(
              applicationId: params['applicationId'],
              cipher: routing.POST['data']
            )
  
            if result.success?
              data = result.value!

              future = Concurrent::Promises.future(0.1) do |duration|
                SetupProcessResult.call(client: @client, data: data)
              end

              view :pay, locals: {
                data: data
              }
            else
                view :error, locals: {
                  message: result.failure
                }
            end
          end
        end
      end
    end
  end
end
