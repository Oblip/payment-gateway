require 'rbnacl'
require 'base64'

def openstruct_to_hash(object, hash = {})
  object.each_pair do |key, value|
    hash[key] = value.is_a?(OpenStruct) ? openstruct_to_hash(value) : value
  end
  hash
end

def encryptMessage(message, secret)
  key = Base64.strict_decode64(secret)
  secret_box = RbNaCl::SecretBox.new(key)
  nonce = RbNaCl::Random.random_bytes(secret_box.nonce_bytes)
  ciphertext = secret_box.encrypt(nonce, message.to_json)
  "#{Base64.strict_encode64(ciphertext)}.#{Base64.strict_encode64(nonce)}"
end