# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:applications) do
      drop_column :name
      add_column :redirect, 'text'
    end
  end
end
