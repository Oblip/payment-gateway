# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:applications) do
      add_column :action_url, 'text'
    end
  end
end
