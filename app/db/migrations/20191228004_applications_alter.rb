# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:applications) do
      rename_column :redirect, :redirect_url
    end
  end
end
