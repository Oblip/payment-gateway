# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:applications) do
      add_primary_key [:id]
    end
  end
end
