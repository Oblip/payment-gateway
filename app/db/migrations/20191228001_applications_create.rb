# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  up do
    create_table(:applications) do
      uuid :id

      String :name
      String :description
      String :username
      String :secret_secure
      String :status
      DateTime :created_at
      DateTime :updated_at
    end
  end
end