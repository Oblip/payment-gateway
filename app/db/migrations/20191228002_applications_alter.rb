# frozen_string_literal: true

require 'sequel'

Sequel.migration do
  change do
    alter_table(:applications) do
      drop_column :status
      add_column :is_active, 'boolean'
      set_column_default :is_active, true
    end
  end
end
