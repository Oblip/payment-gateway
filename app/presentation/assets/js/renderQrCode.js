
function renderQrCode (username, amt, asset, identifier) {

  let url =  `https://oblip.me/${username}?action=request_review`
  url += `&partner=false&amount=${amt}&asset=${asset}&type=payment&identifier=${identifier}`

  QrCodeWithLogo.toCanvas({
    canvas: document.getElementById('canvas'),
    content: url,
    width: 200,
   //  logo: {
   //    src: '/images/logo.jpg',
   //    logoRadius: 100,
   //    borderSize: 0,
   //    logoSize: 0.15
   //  }
  });
}