# frozen_string_literal: true

require 'json'
require 'sequel'

module PaymentGateway
  module Model
    # Models an Application
    class Application < Sequel::Model
      plugin :uuid, field: :id
      plugin :timestamps, update_on_create: true

      def secret=(text)
        self.secret_secure = SecureDB.encrypt(text)
      end

      def secret
        SecureDB.decrypt(secret_secure)
      end
  end
  end
end
