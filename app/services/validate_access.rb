# frozen_string_literal: true

require 'dry-monads'
require 'dry-transaction'
require 'rbnacl'
require 'base64'
require 'json'
require 'ostruct'
require 'rest-client'

module PaymentGateway
  # Service to validate the application and request
  # Usage:
  #   result = ValidateAccesss.new.call(applicationId:, cipher:)
  #   result.success?
  class ValidateAccess
    include Dry::Transaction

    module Errors
      class NotFound < StandardError; end
      class Unauthorized < StandardError; end
      class InvalidPayload < StandardError; end
      class MissingValues < StandardError; end
      class InternalError < StandardError; end
    end
    
    step :check_app_exists
    step :check_is_active
    step :decrypt_cipher
    step :check_required_data
    step :generate_identifier
    step :fetch_account_info
    step :build


    def check_app_exists(applicationId:, cipher:)
      application = Model::Application.find(id: applicationId)
      application.freeze

      raise Errors::NotFound unless application

      Success(application: application, cipher: cipher)
    rescue Errors::NotFound
      Failure('Application does not exist')
    end

    def check_is_active(application:, cipher:)
      raise Errors::Unauthorized unless application.is_active

      Success(app: application, cipher: cipher)
    rescue Errors::Unauthorized
      Failure('Unauthorized')
    end

    def decrypt_cipher(app:, cipher:)
      secret = app.secret
      key = Base64.strict_decode64(secret)
      message, nonce = cipher.split('.')
      
      message = Base64.strict_decode64(message)
      nonce = Base64.strict_decode64(nonce)

      secret_box = RbNaCl::SecretBox.new(key)
      payload = secret_box.decrypt(nonce, message)

      payload = JSON.parse(payload)
      Success(app: app, payload: payload)
    rescue RbNaCl::CryptoError
      Failure('Invalid payload')
    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure('Something went wrong')
    end

    def check_required_data(app:, payload:)
      raise Errors::MissingValues if [payload['amount'], payload['asset']].include?(nil)
        
      Success(app: app, payload: payload)
    rescue Errors::MissingValues
      Failure('Missing required fields')
    end

    def generate_identifier(app:, payload:)
      key = RbNaCl::Random.random_bytes
      identifier = key.unpack('H*')[0]
      

      Success(app: app, payload: payload, identifier: identifier)
    end

    def fetch_account_info(app:, payload:, identifier:)
      url = "#{App.config.OBLIP_API}/v1/account_availability/#{app.username}"
      response = RestClient.get url
      raise Errors::InternalError unless response.code == 200

      account = JSON.parse(response.body)
      Success(app: app, payload: payload, identifier: identifier, account: account)
    
    rescue RestClient::NotFound
      Failure('User does not exist')
    rescue Errors::InternalError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure('Something went wrong.')
    end

    def build(app:, payload:, identifier:, account:)
      data = {
        application: OpenStruct.new({
          id: app.id,
          redirect_url: app.redirect_url,
          action_url: app.action_url,
          secret: app.secret
        }),
        account: OpenStruct.new(account),
        payload: OpenStruct.new(payload),
        identifier: identifier
      }

      result = OpenStruct.new(data)

      Success(result)

    rescue StandardError => error
      puts error.message
      puts error.inspect
      puts error.backtrace
      Failure('Something went wrong')
    end
  end
end
