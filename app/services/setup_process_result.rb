# frozen_string_literal: true

require 'dry-monads'
require 'dry/transaction'
require 'faye'
require 'eventmachine'
require 'rest-client'
require 'rbnacl'
require 'net/http'

module PaymentGateway
  # Service to validate the application and request
  # Usage:
  #   result = SetupProcessResult.new.call(routing:, data:)
  #   result.success?
  module SetupProcessResult
    extend Dry::Monads::Result::Mixin

    def self.call(client:, data:)
      
      EM.run {
        tx_sub = nil
        
        puts "API URL: #{App.config.OBLIP_API}"
        puts "Running in the EM. Identifer /#{data.identifier}"
        tx_sub = client.subscribe("/#{data.identifier}") do |message|
          puts "We are in the subscrbe function"
          puts message
          if !message['http_status']
            begin
              result = {
                status: 'PAID',
                payment_result: message,
                payload: openstruct_to_hash(data.payload)
              }
  
              cipher = encryptMessage(result, data.application.secret)
              res = RestClient.post data.application.action_url, { 'result' => cipher }.to_json, {content_type: :json, accept: :json}
              puts "res code #{res.code}"
              if res.code == 201
                body = JSON.parse(res.body)     
                puts res.body
                url = "#{data.application.redirect_url}?ref=#{body['id']}"
               
                identifier = "gateway-process-complete-#{data.identifier}"
  
                publication = client.publish("/#{identifier}", {
                  redirect_url: url
                })
  
                tx_sub.cancel()
                
              end
            rescue StandardError => e
              puts e.message
            end
          else
            puts "We didn't get any message :("
          end
        end
      }

      Success('Listener is up!')
    end
  end
end