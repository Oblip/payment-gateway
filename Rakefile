# frozen_string_literal: true

require './require_app'

desc 'Run application in development mode and port'
task :run_dev do
  sh 'rerun -c "rackup -p 3001"'
end

task :print_env do
  puts "Environment: #{ENV['RACK_ENV'] || 'development'}"
end

desc 'Run application console (pry)'
task :console => :print_env do
  # sh 'pry -r ./specs/test_load_all'
  sh 'pry -r ./require_app.rb'
end

namespace :generate do
  task(:load_libs) { require_app 'lib' }

  desc 'Create sample cryptographic key for database'
  task :db => :load_libs do
    puts "DB_KEY: #{SecureDB.generate_key}"
  end

  desc 'Create 32 bytes key'
  task :secret32 => :load_libs do
    puts SecureDB.generate_key32
  end
end


namespace :db do
  require_app(nil) # loads config code files only
  require 'sequel'

  Sequel.extension :migration
  app = PaymentGateway::App

  desc 'Run migrations'
  task :migrate => :print_env do
    puts 'Migrating database to latest'
    Sequel::Migrator.run(app.DB, 'app/db/migrations')
  end

end
