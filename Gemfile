# frozen_string_literal: false

source 'https://rubygems.org'
ruby '2.6.2'

# Web
gem 'puma'
gem 'roda'
gem 'slim'
gem 'rack-cors'
gem 'repost'

# Networking
gem 'rest-client'

# Pub/Sub
gem 'faye', '~> 1.2.4'

# Configuration
gem 'econfig'
gem 'rake'

# Asynchronicity gems
gem 'concurrent-ruby'

# Diagnostic
gem 'pry'
gem 'rack-test'

# Database
gem 'hirb'
gem 'sequel'
gem 'pg'

# Services
gem 'dry-monads'
gem 'dry-transaction'
gem 'base32'

# Security
gem 'rbnacl'
gem 'rack-ssl-enforcer'
gem 'secure_headers'

# Testing
group :test do
  gem 'minitest'
  gem 'minitest-rg'
  gem 'simplecov'
  gem 'webmock'
end

# Development
group :development do
  gem 'rubocop'
  gem 'rubocop-performance'
end

group :development do
  gem 'rerun'
end